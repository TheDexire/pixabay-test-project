//
//  ApiRouter.swift
//  Pixabay
//
//  Created by MacBook Pro on 03.03.2022.
//

import Foundation
import Alamofire

typealias Parameters = [String: Any]

enum ApiRouter {
    
    case images(page: Int, category: ImageCategory, query: String)
    case videos(page: Int, category: ImageCategory, query: String)
    
    var method: HTTPMethod {
        return .get
    }
    
    var path: String {
        switch self {
        case .images(let page, let category, let query):
            return AppURLs.pixabayURl + ApiPath.images.rawValue + "?key=\(AppURLs.pixabayApiKey)" + "&category=\(category)" + "&page=\(page)" + "&q=\(query)"
        case .videos(let page, let category, let query):
            return AppURLs.pixabayURl + ApiPath.videos.rawValue + "?key=\(AppURLs.pixabayApiKey)" + "&category=\(category)" + "&page=\(page)" + "&q=\(query)"
        }
    }
    
    var encoding: ParameterEncoding {
        return URLEncoding.default
    }
}
