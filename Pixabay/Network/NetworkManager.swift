//
//  NetworkManager.swift
//  Pixabay
//
//  Created by MacBook Pro on 03.03.2022.
//

import Alamofire
import Foundation

class NetworkManager {
    
    static let shared = NetworkManager()
    
    func networkRequest<T: Codable>(router: ApiRouter, completion: @escaping(Result<T, Error>) -> Void) {
        AF.request(router.path, method: router.method, encoding: URLEncoding.default){ $0.timeoutInterval = 10 }.responseData { (response) in
            switch response.result {
            case .success(let jsonData):
                let decoder = JSONDecoder()
                do {
                    let value = try decoder.decode(T.self, from: jsonData)
                    completion(.success(value))
                } catch let error {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion(.failure(error))
            }
        }
    }
}
