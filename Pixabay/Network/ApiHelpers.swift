//
//  ApiHelpers.swift
//  Pixabay
//
//  Created by MacBook Pro on 03.03.2022.
//

import Foundation

struct AppURLs {
    static let pixabayURl = "https://pixabay.com/api"
    static let pixabayApiKey = "25963337-bad89ab8144cad4a531e9fc5e"
}

enum ApiPath: String, CaseIterable {
    // Images
    case images = "/"
    // Videos
    case videos = "/videos"
}
