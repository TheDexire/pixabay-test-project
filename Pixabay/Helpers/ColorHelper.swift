//
//  ColorHelper.swift
//  Pixabay
//
//  Created by MacBook Pro on 03.03.2022.
//

import UIKit

enum colorDict: String {
    case lightGreenColor = "lightGreenColor"
    case lightBlueColor = "lightBlueColor"
    case customBlackColor = "customBlackColor"
}

extension UIColor
{
    class var lightGreenColor: UIColor {
        return UIColor(named: colorDict.lightGreenColor.rawValue)!
    }
    class var lightBlueColor: UIColor {
        return UIColor(named: colorDict.lightBlueColor.rawValue)!
    }
    class var customBlackColor: UIColor {
        return UIColor(named: colorDict.customBlackColor.rawValue)!
    }
}
