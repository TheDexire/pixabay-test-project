//
//  FontHelper.swift
//  Pixabay
//
//  Created by MacBook Pro on 03.03.2022.
//

import UIKit

private let SFProTextFamily = "SFProText"
private let SFProDisplayFamily = "SFProDisplay"

enum SFProTextFont: String {
    case regular = "Regular"
    case medium = "Medium"
    case semibold = "SemiBold"

    func size(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: fullFontName, size: size) {
            return font
        }
        fatalError("Font '\(fullFontName)' does not exist.")
    }
    private var fullFontName: String {
        return rawValue.isEmpty ? SFProTextFamily : SFProTextFamily + "-" + rawValue
    }
}

enum SFProDisplayFont: String {
    case medium = "Medium"

    func size(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: fullFontName, size: size) {
            return font
        }
        fatalError("Font '\(fullFontName)' does not exist.")
    }
    private var fullFontName: String {
        return rawValue.isEmpty ? SFProDisplayFamily : SFProDisplayFamily + "-" + rawValue
    }
}
