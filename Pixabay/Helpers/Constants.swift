//
//  Constants.swift
//  Pixabay
//
//  Created by MacBook Pro on 03.03.2022.
//

import Foundation

enum ImageCategory: String, CaseIterable {
    case backgrounds = "backgrounds"
    case nature = "nature"
    case science = "science"
    case education = "education"
    case feelings = "feelings"
    case places = "places"
    case industry = "industry"
    case computer = "computer"
    case food = "food"
    case transportation = "transportation"
    case travel = "travel"
    case buildings = "buildings"
    case business = "business"
    
    static func random() -> ImageCategory {
        return ImageCategory.allCases.randomElement()!
    }
}

enum MediaType {
    case image
    case video
}

enum SectionType {
    case images
    case videos
}
