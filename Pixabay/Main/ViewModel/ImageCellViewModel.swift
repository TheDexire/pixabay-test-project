//
//  ImageCellViewModel.swift
//  Pixabay
//
//  Created by MacBook Pro on 03.03.2022.
//

import Foundation

class ImageCellViewModel: MediaCellViewModelType {
    
    private var imageData: ImageHit
    
    var title: String {
        return imageData.user ?? ""
    }
    
    var image: URL? {
        let imageUrl = URL(string: imageData.previewURL ?? "")
        return imageUrl!
    }
    
    var type: MediaType {
        return .image
    }
    
    init(imageData: ImageHit) {
        self.imageData = imageData
    }
}
