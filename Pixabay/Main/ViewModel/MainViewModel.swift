//
//  MainViewModel.swift
//  Pixabay
//
//  Created by MacBook Pro on 03.03.2022.
//

import Foundation

class MainViewModel: NSObject, MainViewModelType {
    
    private var networkManager = NetworkManager()
    private let randomCategory = ImageCategory.random()
    
    var images = Images()
    var videos = Videos()
    
    func fetchImages(page: Int = 1, query: String = "", completion: @escaping() -> ()) {
        networkManager.networkRequest(router: .images(page: page, category: randomCategory, query: query)) { (_ result: Result<Image, Error>) in
            switch result {
            case .success(let data):
                if let images = data.hits  {
                    for image in images {
                        self.images.append(image)
                    }
                    completion()
                }
            case .failure(_):
                completion()
            }
        }
    }
    
    func fetchVideos(page: Int = 1, query: String = "", completion: @escaping() -> ()) {
        networkManager.networkRequest(router: .videos(page: page, category: randomCategory, query: query)) { (_ result: Result<Video, Error>) in
            switch result {
            case .success(let data):
                if let videos = data.hits  {
                    for video in videos {
                        self.videos.append(video)
                    }
                    completion()
                }
            case .failure(_):
                completion()
            }
        }
    }
    
    var numberOfImageItems: Int {
        return images.count
    }
    
    var numberOfVideoItems: Int {
        return videos.count
    }
    
    func imageCellViewModel(for indexPath: IndexPath) -> MediaCellViewModelType? {
        let image = images[indexPath.row]
        return ImageCellViewModel(imageData: image)
    }
    
    func videoCellViewModel(for indexPath: IndexPath) -> MediaCellViewModelType? {
        let video = videos[indexPath.row]
        return VideoCellViewModel(videoData: video)
    }
}
