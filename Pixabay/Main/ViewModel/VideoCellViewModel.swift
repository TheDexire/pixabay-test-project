//
//  VideoCellViewModel.swift
//  Pixabay
//
//  Created by MacBook Pro on 05.03.2022.
//

import Foundation

class VideoCellViewModel: MediaCellViewModelType {
    
    private var videoData: VideoHit
    
    var title: String {
        return videoData.user ?? ""
    }
    
    var image: URL? {
        return nil
    }
    
    var type: MediaType {
        return .video
    }
    
    init(videoData: VideoHit) {
        self.videoData = videoData
    }
}
