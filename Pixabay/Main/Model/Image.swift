//
//  Image.swift
//  Pixabay
//
//  Created by MacBook Pro on 03.03.2022.
//

import Foundation

typealias Images = [ImageHit]

// MARK: - Image
struct Image: Codable {
    let total, totalHits: Int?
    let hits: [ImageHit]?
}

// MARK: - Hit
struct ImageHit: Codable {
    let id: Int?
    let pageURL: String?
    let type: ImageType?
    let tags: String?
    let previewURL: String?
    let previewWidth, previewHeight: Int?
    let webformatURL: String?
    let webformatWidth, webformatHeight: Int?
    let largeImageURL: String?
    let imageWidth, imageHeight, imageSize, views: Int?
    let downloads, collections, likes, comments: Int?
    let userID: Int?
    let user: String?
    let userImageURL: String?

    enum CodingKeys: String, CodingKey {
        case id, pageURL, type, tags, previewURL, previewWidth, previewHeight, webformatURL, webformatWidth, webformatHeight, largeImageURL, imageWidth, imageHeight, imageSize, views, downloads, collections, likes, comments
        case userID = "user_id"
        case user, userImageURL
    }
}

enum ImageType: String, Codable {
    case illustration = "illustration"
    case photo = "photo"
    case vectorSVG = "vector/svg"
    case vectorAI = "vector/ai"
}
