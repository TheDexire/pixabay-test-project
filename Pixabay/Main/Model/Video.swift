//
//  Video.swift
//  Pixabay
//
//  Created by MacBook Pro on 04.03.2022.
//

import Foundation

typealias Videos = [VideoHit]

// MARK: - Video
struct Video: Codable {
    let total, totalHits: Int?
    let hits: [VideoHit]?
}

// MARK: - Hit
struct VideoHit: Codable {
    let id: Int?
    let pageURL: String?
    let type: VideoType?
    let tags: String?
    let duration: Int?
    let pictureID: String?
    let videos: VideoSizeType?
    let views, downloads, likes, comments: Int?
    let userID: Int?
    let user: String?
    let userImageURL: String?

    enum CodingKeys: String, CodingKey {
        case id, pageURL, type, tags, duration
        case pictureID = "picture_id"
        case videos, views, downloads, likes, comments
        case userID = "user_id"
        case user, userImageURL
    }
}

enum VideoType: String, Codable {
    case animation = "animation"
    case film = "film"
}

// MARK: - Videos
struct VideoSizeType: Codable {
    let large, medium, small, tiny: VideoData?
}

// MARK: - Large
struct VideoData: Codable {
    let url: String?
    let width, height, size: Int?
}
