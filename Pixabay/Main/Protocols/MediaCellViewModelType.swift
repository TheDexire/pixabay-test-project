//
//  MediaCellViewModelType.swift
//  Pixabay
//
//  Created by MacBook Pro on 05.03.2022.
//

import Foundation

protocol MediaCellViewModelType: AnyObject {
    var title: String { get }
    var image: URL? { get }
    var type: MediaType { get }
}
