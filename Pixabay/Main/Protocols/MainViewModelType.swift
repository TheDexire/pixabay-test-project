//
//  MainViewModelType.swift
//  Pixabay
//
//  Created by MacBook Pro on 05.03.2022.
//

import Foundation

protocol MainViewModelType {
    var numberOfImageItems: Int { get }
    var numberOfVideoItems: Int { get }
    func imageCellViewModel(for indexPath: IndexPath) -> MediaCellViewModelType?
    func videoCellViewModel(for indexPath: IndexPath) -> MediaCellViewModelType?
}
