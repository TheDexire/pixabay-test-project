//
//  DetailImageController.swift
//  Pixabay
//
//  Created by MacBook Pro on 05.03.2022.
//

import UIKit
import Kingfisher

class DetailImageController: UIViewController {
    
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var detailImageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var imagePath: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showImage(with: imagePath)
    }
    
    private func showImage(with path: String?) {
        guard let imagePath = path else { return }
        let imageUrl = URL(string: imagePath)
        detailImageView.kf.setImage(with: imageUrl) { [weak self] result in
            switch result {
            case .success(let value):
                self?.resizeImage(with: value.image)
                self?.spinner.stopAnimating()
            case .failure(_): break
            }
        }
    }
    
    private func resizeImage(with image: UIImage) {
        let ratio = image.size.width / image.size.height
        let containerWidth = self.detailImageView.frame.width
        let newHeight = containerWidth / ratio
        self.detailImageViewHeight.constant = newHeight
    }
    
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
