//
//  MainController.swift
//  Pixabay
//
//  Created by MacBook Pro on 03.03.2022.
//

import UIKit
import AVKit

class MainController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var viewModel: MainViewModel!
    var visibleSection: SectionType = .images
    var imagesPage = 1
    var videosPage = 1
    var searchText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MainViewModel()
        configurateView()
        configurateKeyboard()
        getImages()
        getVideos()
    }
    // MARK: - Get Media Data
    // get images
    fileprivate func getImages(with query: String = "", page: Int = 1) {
        viewModel.fetchImages(page: page, query: query) { [weak self] in
            self?.spinner.stopAnimating()
            self?.collectionView.reloadData()
        }
    }
    // get videos
    fileprivate func getVideos(with query: String = "", page: Int = 1) {
        viewModel.fetchVideos(page: page, query: query) { [weak self] in
            self?.spinner.stopAnimating()
            self?.collectionView.reloadData()
        }
    }
    // MARK: - UI Configuration
    // View configuration
    private func configurateView() {
        // media cell register
        collectionView.register(MediaCell.nib, forCellWithReuseIdentifier: MediaCell.identifier)
        // search view register
        collectionView.register(UINib(nibName: SearchView.identifier.description, bundle: nil),
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: SearchView.identifier)
        // dismiss keyboard if collectionview begin drag
        collectionView.keyboardDismissMode = .onDrag
        // spinner color set
        spinner.color = .customBlackColor
    }
    // prepare for update collectionView
    fileprivate func prepareForUpdateCollectionView() {
        hideKeyboard()
        spinner.startAnimating()
        UIView.performWithoutAnimation {
            collectionView.reloadSections(IndexSet(integer: 1))
        }
    }
    // change visible section
    fileprivate func changeSection(to index: Int) {
        visibleSection = index == 0 ? .images : .videos
        collectionView.reloadSections(IndexSet(integer: 1))
    }
    // keyboard configuration
    private func configurateKeyboard() {
        // add tap gesture recognizer for hide keyboard
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    // hide keyboard function
    @objc private func hideKeyboard() {
        self.view.endEditing(true)
    }
    // MARK: - Play Video On FullScreen Method
    fileprivate func playVideo(url: URL) {
        let player = AVPlayer(url: url)
        let vc = AVPlayerViewController()
        vc.player = player
        self.present(vc, animated: true) { vc.player?.play() }
    }
    // MARK: - Prepare For Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showImageSegue" {
            if let indexPath = collectionView.indexPathsForSelectedItems?.first{
                let selectedRow = indexPath.row
                guard let detailImageController = segue.destination as? DetailImageController else { return }
                detailImageController.imagePath = self.viewModel.images[selectedRow].largeImageURL
            }
        }
    }
    // MARK: - Change Orientation Update CollectionView Cells
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.invalidateLayout()
        }
    }
}

extension MainController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDataSourcePrefetching {
    // MARK: - CollectionView DataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 1:
            if visibleSection == .images {
                return viewModel.numberOfImageItems
            } else {
                return viewModel.numberOfVideoItems
            }
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            return UICollectionViewCell()
        default:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MediaCell.identifier, for: indexPath) as? MediaCell else { fatalError("xib does not exists") }
            if visibleSection == .images {
                let cellViewModel = viewModel.imageCellViewModel(for: indexPath)
                cell.viewModel = cellViewModel
            } else {
                let cellViewModel = viewModel.videoCellViewModel(for: indexPath)
                cell.viewModel = cellViewModel
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let searchView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SearchView.identifier, for: indexPath) as? SearchView else { fatalError("xib does not exists") }
        
        searchView.segmentAction = { [weak self] index in
            self?.changeSection(to: index)
        }
        searchView.searchBarSearchButtonTapped = { [weak self] searchText in
            self?.searchText = searchText
            switch self?.visibleSection {
            case .images:
                self?.viewModel.images.removeAll()
                self?.prepareForUpdateCollectionView()
                self?.getImages(with: searchText)
            case .videos:
                self?.viewModel.videos.removeAll()
                self?.prepareForUpdateCollectionView()
                self?.getVideos(with: searchText)
            case .none: break
            }
        }
        
        return searchView
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            switch visibleSection {
            case .images:
                if indexPath.row == viewModel.numberOfImageItems - 1 {
                    self.imagesPage += 1
                    self.getImages(with: self.searchText, page: imagesPage)
                }
            case .videos:
                if indexPath.row == viewModel.numberOfVideoItems - 1 {
                    self.videosPage += 1
                    self.getVideos(with: self.searchText, page: videosPage)
                }
            }
        }
    }
    // MARK: - CcollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return section == 0 ? CGSize(width: collectionView.frame.width, height: 133) : .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? MediaCell else { return }
        let mediaType = cell.viewModel?.type
        let selectedRow = indexPath.row
        switch mediaType {
        case .image:
            performSegue(withIdentifier: "showImageSegue", sender: nil)
        case .video:
            let videoPath = self.viewModel.videos[selectedRow].videos?.medium?.url // отображение видео в среднем качестве, по желанию можно сделать large
            guard let videoPath = videoPath, let videoUrl = URL(string: videoPath) else { return }
            self.playVideo(url: videoUrl)
        case .none: break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let horizontalMargin: CGFloat = 40
        let spaceBetweenCells: CGFloat = 20
        let collectionViewWidth = collectionView.bounds.width - horizontalMargin - spaceBetweenCells
        return CGSize(width: collectionViewWidth/2, height: 200)
    }
}
