//
//  SearchView.swift
//  Pixabay
//
//  Created by MacBook Pro on 04.03.2022.
//

import UIKit

class SearchView: UICollectionReusableView, UISearchBarDelegate {
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var searchBarView: UISearchBar!
    
    class var identifier: String { return String(describing: self) }
    
    var segmentAction: ((_ index: Int)->())?
    var searchBarSearchButtonTapped: ((_ searchText: String)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        searchBarView.delegate = self
    }
    
    @IBAction func segmentControlTapped(_ sender: UISegmentedControl) {
        segmentAction?(sender.selectedSegmentIndex)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text else { return }
        searchBarSearchButtonTapped?(searchText)
    }
}
