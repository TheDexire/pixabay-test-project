//
//  MediaCell.swift
//  Pixabay
//
//  Created by MacBook Pro on 03.03.2022.
//

import UIKit
import Kingfisher

class MediaCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: MainLabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var playImageView: UIImageView!
    
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
    // ViewModel
    var viewModel: MediaCellViewModelType? {
        willSet(viewModel) {
            guard let viewModel = viewModel else { return }
            titleLabel.text = viewModel.title
            
            if viewModel.type == .image {
                self.backgroundColor = .lightGreenColor
                imageView.kf.setImage(with: viewModel.image)
                playImageView.isHidden = true
                titleLabel.textColor = .black
            } else if viewModel.type == .video {
                self.backgroundColor = .lightBlueColor
                playImageView.isHidden = false
                titleLabel.textColor = .customBlackColor
            }
        }
    }
    // Awake From Nib
    override func awakeFromNib() {
        super.awakeFromNib()
        viewsConfiguration()
    }
    // Configurate Views
    private func viewsConfiguration() {
        self.layer.cornerRadius = 8
        imageView.contentMode = .scaleAspectFill
    }
    // Reuse Cell Method
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        titleLabel.text = ""
        playImageView.isHidden = true
    }
}
