//
//  ComponentClasses.swift
//  Pixabay
//
//  Created by MacBook Pro on 03.03.2022.
//

import UIKit

class MainLabel: UILabel {
    
    override func awakeFromNib() {
        textColor = .black
        font = SFProTextFont.regular.size(17)
    }
}

class MainButton: UIButton {
    override func awakeFromNib() {
        titleLabel?.textColor = .white
        titleLabel?.font = SFProTextFont.medium.size(28)
    }
    
    override var isHighlighted: Bool {
        didSet {
            UIView.animate(withDuration: 0.1, delay: 0, options: [.beginFromCurrentState, .allowUserInteraction], animations: {
                self.titleLabel?.alpha = self.isHighlighted ? 0.5 : 1
            }, completion: nil)
        }
    }
}

class MainNavigationBar: UINavigationBar {
    override func awakeFromNib() {
        let attributes = [NSAttributedString.Key.font: SFProDisplayFont.medium.size(20)]
        UINavigationBar.appearance().titleTextAttributes = attributes
    }
}

class MainSegmentedControl: UISegmentedControl {
    override func awakeFromNib() {
        setTitleTextAttributes([NSAttributedString.Key.font: SFProTextFont.semibold.size(15)], for: .normal)
    }
}
